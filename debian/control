Source: libpreludedb
Priority: optional
Section: libs
Maintainer: Pierre Chifflier <pollux@debian.org>
Uploaders: Thomas Andrejak <thomas.andrejak@gmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
    dh-sequence-python3 <!nopython>,
    libprelude-dev (>=5.2.0),
    libpq-dev,
    default-libmysqlclient-dev,
    libsqlite3-dev,
    pkg-config,
    libpython3-all-dev <!nopython>,
    python3-all-dev:any <!nopython>,
    swig <!nopython>
Build-Depends-Indep: gtk-doc-tools, libglib2.0-dev
Standards-Version: 4.6.0
Homepage: https://www.prelude-siem.org/
Vcs-Browser: https://salsa.debian.org/totol-guest/libpreludedb
Vcs-Git: https://salsa.debian.org/totol-guest/libpreludedb.git

Package: libpreludedb-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Suggests: libpreludedb-doc
Depends: libpreludedb7 (= ${binary:Version}), libpreludedbcpp2 (= ${binary:Version}),
 libprelude-dev, ${misc:Depends}
Description: Security Information and Events Management System [ Development files ]
 The PreludeDB Library provides an abstraction layer upon the type and the
 format of the database used to store IDMEF alerts. It allows developers to use
 the Prelude IDMEF database easily and efficiently without worrying about SQL,
 and to access the database independently of the type/format of the database.
 .
 This package contains the development files for PreludeDB components.

Package: libpreludedb-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Security Information and Events Management System [ Documentation ]
 The PreludeDB Library provides an abstraction layer upon the type and the
 format of the database used to store IDMEF alerts. It allows developers to use
 the Prelude IDMEF database easily and efficiently without worrying about SQL,
 and to access the database independently of the type/format of the database.
 .
 This package contains the documentation for developers for
 PreludeDB components.

Package: libpreludedb7
Architecture: any
Multi-Arch: same
Recommends: preludedb-utils
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Security Information and Events Management System [ Base library ]
 The PreludeDB Library provides an abstraction layer upon the type and the
 format of the database used to store IDMEF alerts. It allows developers to use
 the Prelude IDMEF database easily and efficiently without worrying about SQL,
 and to access the database independently of the type/format of the database.
 .
 This package contains the PreludeDB shared library.

Package: libpreludedbcpp2
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Security Information and Events Management System [ C++ library ]
 The PreludeDB Library provides an abstraction layer upon the type and the
 format of the database used to store IDMEF alerts. It allows developers to use
 the Prelude IDMEF database easily and efficiently without worrying about SQL,
 and to access the database independently of the type/format of the database.
 .
 This package contains the Prelude C++ shared library.

Package: preludedb-utils
Architecture: all
Breaks: libpreludedb0
Replaces: libpreludedb0
Section: utils
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}, python3,
 python3-prelude (>=5.2.0), python3-preludedb (>=5.2.0)
Build-Profiles: <!nopython>
Provides: ${python3:Provides}
Description: Security Information and Events Management System [ Library utils ]
 The PreludeDB Library provides an abstraction layer upon the type and the
 format of the database used to store IDMEF alerts. It allows developers to use
 the Prelude IDMEF database easily and efficiently without worrying about SQL,
 and to access the database independently of the type/format of the database.
 .
 This package contains the Prelude shared library tools.

Package: python3-preludedb
Section: python
Architecture: any
Depends: ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends}, python3-prelude (>=5.2.0)
Build-Profiles: <!nopython>
Provides: ${python3:Provides}
Description: Security Information and Events Management System [ Python3 bindings ]
 The PreludeDB Library provides an abstraction layer upon the type and the
 format of the database used to store IDMEF alerts. It allows developers to use
 the Prelude IDMEF database easily and efficiently without worrying about SQL,
 and to access the database independently of the type/format of the database.
 .
 This package contains the Python 3 bindings for PreludeDB.

Package: libpreludedb7-mysql
Architecture: any
Multi-Arch: same
Breaks: libpreludedb0
Replaces: libpreludedb0
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Security Information and Events Management System [ MySQL library ]
 The PreludeDB Library provides an abstraction layer upon the type and the
 format of the database used to store IDMEF alerts. It allows developers to use
 the Prelude IDMEF database easily and efficiently without worrying about SQL,
 and to access the database independently of the type/format of the database.
 .
 This package contains the PreludeDB shared library for MySQL.

Package: libpreludedb7-pgsql
Architecture: any
Multi-Arch: same
Breaks: libpreludedb0
Replaces: libpreludedb0
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Security Information and Events Management System [ PGSQL library ]
 The PreludeDB Library provides an abstraction layer upon the type and the
 format of the database used to store IDMEF alerts. It allows developers to use
 the Prelude IDMEF database easily and efficiently without worrying about SQL,
 and to access the database independently of the type/format of the database.
 .
 This package contains the PreludeDB shared library for PGSQL.

Package: libpreludedb7-sqlite
Architecture: any
Multi-Arch: same
Breaks: libpreludedb0
Replaces: libpreludedb0
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Security Information and Events Management System [ SQLite library ]
 The PreludeDB Library provides an abstraction layer upon the type and the
 format of the database used to store IDMEF alerts. It allows developers to use
 the Prelude IDMEF database easily and efficiently without worrying about SQL,
 and to access the database independently of the type/format of the database.
 .
 This package contains the PreludeDB shared library for SQLite.
